### => Fichiers :
Fichier install_firewall.yml : playbook ansible permettant l'installation et la configuartion d'un firewall

Fichier install_docker.yml : playbook ansible permettant l'installation de docker et le lancement 

### => Commandes à executer :
- Installer le Firewall:

`ansible-playbook install_firewall.yml -i hosts.ini`

- Installer Docker:

`ansible-playbook install_docker.yml -i hosts.ini`

- Contacter les serveurs en utilisant ansible

`ansible all -m ping -i hosts.ini`
