terraform {
required_version = ">= 0.13.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.35.0"
    }
  }
}

resource "openstack_compute_keypair_v2" "keypair" {
  provider = openstack
  name = "id_rsa"
  public_key  = file("~/.ssh/id_rsa.pub")
}

resource "openstack_compute_instance_v2" "instancesU" {
	 count = 2
	 name = "instanceTerraform_${count.index}"
	 provider = openstack
	 image_name = "ubuntu-20.04"
	 flavor_name = "normale"
	 key_pair = openstack_compute_keypair_v2.keypair.name
}

resource "openstack_compute_instance_v2" "instanceC" {
  count = 2
  name = "instanceCentos_${count.index}"
  provider = openstack
  image_name = "centos-7"
  flavor_name = "normale"
  key_pair = openstack_compute_keypair_v2.keypair.name
}